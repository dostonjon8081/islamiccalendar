/*
package uz.def.islamicCalendar.prayer

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_prayer.*
import uz.def.islamicCalendar.R

class PrayerListAdapter(
    private val listModel: MutableList<PrayerListModel>,
    private val itemClick: ItemClick
) : RecyclerView.Adapter<PrayerListAdapter.PrayerAdapterVH>() {

    val list = mutableListOf<PrayerListModel>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PrayerAdapterVH {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_prayer, parent, false)

        return PrayerAdapterVH(view, itemClick)
    }

    override fun getItemCount(): Int {
        return listModel.size
    }

    override fun onBindViewHolder(holder: PrayerAdapterVH, position: Int) {
        holder.onBind(listModel[position])
    }

    class PrayerAdapterVH(override val containerView: View?, private val itemClick: ItemClick) :
        RecyclerView.ViewHolder(containerView!!), LayoutContainer {

        fun onBind(model: PrayerListModel) {
            tv_prayer.text = model.name

            model.imageId?.let { iv_name.setImageResource(it) }

            containerView!!.setOnClickListener { itemClick.onItemClick(adapterPosition) }
        }

    }

}*/
