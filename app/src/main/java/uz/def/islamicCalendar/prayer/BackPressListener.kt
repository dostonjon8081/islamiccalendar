package uz.def.islamicCalendar.prayer

interface BackPressListener {
    fun onBackPress():Boolean
}