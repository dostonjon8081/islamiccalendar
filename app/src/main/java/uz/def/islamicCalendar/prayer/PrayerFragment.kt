package uz.def.islamicCalendar.prayer

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.util.Log
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_prayer.*
import retrofit2.http.Url
import uz.def.islamicCalendar.R
import uz.def.islamicCalendar.ui.fragment.BaseFragment

class PrayerFragment : BaseFragment(), BackPressListener {
    /* val list = mutableListOf<PrayerListModel>()
     var adapter: PrayerListAdapter? = null
 */
    override fun getLayout(): Int {
        return R.layout.fragment_prayer
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun setTitle() {

        wv_prayer.webViewClient = WebViewClient()

        wv_prayer.settings.allowFileAccess = true
        wv_prayer.settings.supportZoom()
        wv_prayer.settings.setAppCacheEnabled(true)
        wv_prayer.settings.databaseEnabled = true
        wv_prayer.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        wv_prayer.settings.allowContentAccess = true
        wv_prayer.settings.javaScriptEnabled = true

        if (isOnline()) {
            wv_prayer.loadUrl("https://namoz.islom.uz/")
        } else {
            Toast.makeText(requireContext(), "you have not network", Toast.LENGTH_LONG).show()
        }

        /*generateList()
        Log.d("forRecyclerView",list.toString())
        adapter = PrayerListAdapter(list, this)

        rv_prayer.adapter = adapter*/
    }

    /*private fun generateList() {
        list.add(PrayerListModel("Tahorat", R.drawable.ic_water, "https://namoz.islom.uz/"))
        list.add(
            PrayerListModel(
                "Namoz",
                R.drawable.ic_prayer_man,
                "https://namoz.islom.uz/namoz.html"
            )
        )
        list.add(
            PrayerListModel(
                "Suralar",
                R.drawable.ic_quran,
                "https://namoz.islom.uz/suralar.html"
            )
        )
        list.add(
            PrayerListModel(
                "Savollar",
                R.drawable.ic_conversation,
                "https://namoz.islom.uz/savollar.html"
            )
        )
    }*/

    override fun setViews() {
    }

    /*override fun onItemClick(position: Int) {

        wv_prayer.visibility = View.VISIBLE
        rv_prayer.visibility = View.GONE

        wv_prayer.loadUrl(list[position].url!!)
    }*/


    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context = requireContext()): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    return true
                }
            }
        }
        return false
    }

    override fun onBackPress(): Boolean {
        return if (onBackPress()) {

            wv_prayer.visibility = View.GONE
            rv_prayer.visibility = View.VISIBLE
            true
        } else false
    }

}