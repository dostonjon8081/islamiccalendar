package uz.def.islamicCalendar.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import uz.def.islamicCalendar.ui.activity.BaseActivity

abstract class BaseFragment : Fragment() {

    var rootView: View? = null
    var layout: Int? = getLayout()

    abstract fun getLayout(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = layout?.let { inflater.inflate(it, container,false) }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle()
        setViews()
    }


    abstract fun setTitle()
    abstract fun setViews()

    fun getBaseActivity(run: (BaseActivity) -> Unit) {
        (activity as BaseActivity)?.let {
            it?.let {
                run(it)
            }
        }
    }

}

