package uz.def.islamicCalendar.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.LocationManager
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.observe
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import uz.def.islamicCalendar.R
import uz.def.islamicCalendar.compass.CompassActivity
import uz.def.islamicCalendar.firstInit.FirstInitFragment
import uz.def.islamicCalendar.helper.MY_PERMISSIONS_REQUEST_LOCATION
import uz.def.islamicCalendar.helper.RESOLVE_REQUEST_LOCATION
import uz.def.islamicCalendar.helper.db.PrayerTimesModelDb
import uz.def.islamicCalendar.helper.mGetColor
import uz.def.islamicCalendar.home.HomeFragment
import uz.def.islamicCalendar.prayer.PrayerFragment
import uz.def.islamicCalendar.setting.SettingFragment
import uz.def.islamicCalendar.ui.fragment.SplashFragment


@Suppress("DEPRECATION")
class MainActivity : BaseActivity() {

    private val settingFragment by lazy(LazyThreadSafetyMode.NONE) { SettingFragment() }
    var backPress: Boolean = false

    private val fusedLocationProviderClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(this)
    }
    private val geocoder by lazy { Geocoder(this) }
    private val lm by lazy(LazyThreadSafetyMode.NONE) { this.getSystemService(Context.LOCATION_SERVICE) as LocationManager }
    private var gps_enabled = false

    override fun getActivityLayout(): Int = R.layout.activity_main

    override fun getFragmentContainerId(): Int = R.id.container

    override fun setupTitle() {

        addFragment(SplashFragment(), getFragmentContainerId())
//        changeOtherIcons()
    }

    override fun setupViews() {
        if (!pref.firstInit) {
            if (isOnline()) {
                checkPermissionLocation()
                addFragment(FirstInitFragment(), getFragmentContainerId())
                /*Intent(this,AzanService::class.java).apply {
                    ContextCompat.startForegroundService(this@MainActivity,this)
                }*/
            } else {
                AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setPositiveButton("Exit") { _, _ -> finish() }
                    .setTitle("Network")
                    .setMessage("You are not network,  enable open this app")
                    .show()
            }
        } else {
//            writeToDatabase()
            /*CoroutineScope(Dispatchers.IO).launch {
                timeDataDbViewModel.allTimeData()}*/
            bottomBarVisibility()
            goHomeLayout()
        }

        homeBtn.setOnClickListener { goHomeLayout() }

        compassBtn.setOnClickListener { goCompass() }

        prayerBtn.setOnClickListener { goPrayerLayout() }

        settingBtn.setOnClickListener { goSettingLayout() }

//        requestApiCalendar()

    }

    fun bottomBarVisibility() {
        if (pref.visiblity_bottom_navigation) {
            bottomNavigation.visibility = View.VISIBLE
        }
    }


    private fun goSettingLayout() {
        changeOtherIcons()

        addFragment(settingFragment, getFragmentContainerId())
        settingImage.setImageResource(R.drawable.ic_settings_select)
        settingText.setTextColor(mGetColor(R.color.common_google_signin_btn_text_dark))
    }

    private fun goPrayerLayout() {
        changeOtherIcons()

        addFragment(PrayerFragment(), getFragmentContainerId())
        prayerImage.setImageResource(R.drawable.ic_prayer_select)
        prayerText.setTextColor(mGetColor(R.color.common_google_signin_btn_text_dark))
    }

    private fun goCompass() {
        changeOtherIcons()

//        startActivity(Intent(this, CompassActivity::class.java))
        addFragment(CompassActivity(), getFragmentContainerId())
        compassImage.setImageResource(R.drawable.ic_compass_select)
        compassText.setTextColor(mGetColor(R.color.common_google_signin_btn_text_dark))
    }

    fun goHomeLayout() {
        timeDataDbViewModel.allTimeData()
        changeOtherIcons()

        addFragment(HomeFragment(), getFragmentContainerId())
        homeImage.setImageResource(R.drawable.ic_home_select)
        homeText.setTextColor(mGetColor(R.color.common_google_signin_btn_text_dark))
    }

    private fun changeOtherIcons() {
        homeText.setTextColor(mGetColor(R.color.bottomText))
        compassText.setTextColor(mGetColor(R.color.bottomText))
        prayerText.setTextColor(mGetColor(R.color.bottomText))
        settingText.setTextColor(mGetColor(R.color.bottomText))

        homeImage.setImageResource(R.drawable.ic_home)
        compassImage.setImageResource(R.drawable.ic_compass)
        prayerImage.setImageResource(R.drawable.ic_prayer)
        settingImage.setImageResource(R.drawable.ic_settings)
    }

    fun firstRequestApiCalendar() {
        CoroutineScope(Dispatchers.IO).launch {
            apiCalendarViewModel?.provideCalendar(
                pref.latitude,
                pref.longitude,
                pref.month,
                pref.year
            )
            /*    timeDataDbViewModel.liveData.observe(this@MainActivity) {


                        Log.d("getCalendar", "in Main Request")
                        apiCalendarViewModel?.provideCalendar(
                            pref.latitude,
                            pref.longitude,
                            pref.month,
                            pref.year
                        )
                }*/
            withContext(Dispatchers.Main) {
                writeToDatabase()
                /* Intent(this@MainActivity, AzanService::class.java).apply {
                     ContextCompat.startForegroundService(this@MainActivity, this)
                 }*/
            }
        }
    }

    private fun writeToDatabase() {

        apiCalendarViewModel.apiCalendar.observe(this@MainActivity) {
            Log.d("getCalendar", "writeToDatabase: $it")
            CoroutineScope(Dispatchers.IO).launch {
                timeDataDbViewModel.clearDb()
                for (index in 0 until it.data.lastIndex + 1) {
                    try {
                        if (it.data[index] != null) {
                            val timeDataModelDb = PrayerTimesModelDb(
                                it.data[index].timings.Fajr.substring(0, 5),
                                it.data[index].timings.Sunrise.substring(0, 5),
                                it.data[index].timings.Dhuhr.substring(0, 5),
                                it.data[index].timings.Asr.substring(0, 5),
                                it.data[index].timings.Maghrib.substring(0, 5),
                                it.data[index].timings.Isha.substring(0, 5),
                                it.data[index].date.gregorian.date,
                                it.data[index].date.gregorian.month.number.toInt()
                            )
                            timeDataDbViewModel.insertTimeData(timeDataModelDb)
                        } else {
                            timeDataDbViewModel.allTimeData()
                            break
                        }
                    } catch (e: Exception) {
                        Log.d("databses", "in api catch")
                    }
                }
            }
        }
    }

    fun setProgressCounter(count: Int, progress: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            apiCalendarViewModel?.runProgress(count, progress)
        }
    }

    override fun onBackPressed() {


        Handler().postDelayed({
            backPress = false
        }, 1000)
        if (backPress) {
            super.onBackPressed()
        } else {
            Toast.makeText(this, "click again back", Toast.LENGTH_LONG).show()
            backPress = true
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RESOLVE_REQUEST_LOCATION) {
            getLatLong()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * location
     * for get current location
     */

    fun checkPermissionLocation() {

        val fineLocationPermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        val coarseLocationPermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        if (fineLocationPermission != PackageManager.PERMISSION_GRANTED && coarseLocationPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ), MY_PERMISSIONS_REQUEST_LOCATION
            )
        } else {
            checkEnableLocation()
        }
    }

    @SuppressLint("MissingPermission")
    fun checkEnableLocation() {

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (e: Exception) {
        }

        if (!gps_enabled) {

            fusedLocationProviderClient.locationAvailability
                .addOnSuccessListener {
                    if (it.isLocationAvailable) {
                        getLatLong()

                    } else {
                        setUpLocationService()
                    }
                }
                .addOnFailureListener {
                    setUpLocationService()

                }

        } else {

            CoroutineScope(Dispatchers.IO).launch {
                getLatLong()
                Thread.sleep(3000)
            }
        }
    }

    private fun setUpLocationService() {

        val request = locationRequest()

        val settingRequest = LocationSettingsRequest.Builder()
            .addLocationRequest(request)
            .build()

        LocationServices.getSettingsClient(this)
            .checkLocationSettings(settingRequest)
            .addOnSuccessListener { getLatLong() }
            .addOnFailureListener { resolveLocationException(it) }
    }

    private fun resolveLocationException(exception: Exception) {
        if (exception is ResolvableApiException) {
            try {
                exception.startResolutionForResult(
                    this,
                    RESOLVE_REQUEST_LOCATION
                )
            } catch (e: Exception) {

            }
        }
    }

    private fun locationRequest() = LocationRequest.create().apply {
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        interval = 10_000
    }

    @SuppressLint("MissingPermission")
    private fun getLatLong() {

        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            LocationServices.getFusedLocationProviderClient(this)
                .requestLocationUpdates(locationRequest(), object : LocationCallback() {
                    override fun onLocationResult(resultLocation: LocationResult) {
                        super.onLocationResult(resultLocation)

                        fusedLocationProviderClient.removeLocationUpdates(this)

                        if (resultLocation != null) {
                            val lastLocationIndex = resultLocation.locations.size - 1

                            pref.city = geocoder.getFromLocation(
                                resultLocation.locations[lastLocationIndex].latitude,
                                resultLocation.locations[lastLocationIndex].longitude,
                                1
                            )[0].getAddressLine(0)
                            Toast.makeText(this@MainActivity, pref.city, Toast.LENGTH_SHORT).show()
                            pref.latitude =
                                resultLocation.locations[lastLocationIndex].latitude.toFloat()
                            pref.longitude =
                                resultLocation.locations[lastLocationIndex].longitude.toFloat()
                            firstRequestApiCalendar()
                            timeDataDbViewModel.allTimeData()
                            pref.firstInit = false
//                            pref.firstInit = false
//                            startActivity(Intent(this@MainActivity,MainActivity::class.java))
//                            finish()
                            goHomeLayout()
                        }
                    }
                }, Looper.getMainLooper())
        } else {
            setUpLocationService()
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (permissions.isNotEmpty() &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            checkEnableLocation()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}