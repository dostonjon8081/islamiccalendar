package uz.def.islamicCalendar.ui.fragment

import android.os.Handler
import kotlinx.android.synthetic.main.fragment_splash.*
import uz.def.islamicCalendar.R
import uz.def.islamicCalendar.helper.animation.animationSplash
import uz.def.islamicCalendar.ui.activity.MainActivity

class SplashFragment : BaseFragment() {


    override fun getLayout(): Int =
        R.layout.fragment_splash

    override fun setViews() {

        Handler().postDelayed({

//            splash_logo.animationSplash(1f, 17f, 300) {

                getBaseActivity {
//                    it.addFragmentFirstTime(HomeFragment(),it.getFragmentContainerId())
                    (it as MainActivity).setupViews()
                }
//                startActivity(Intent(this, MainActivity::class.java))
//            }
        }, 2000)
    }

    override fun setTitle() {
    }
}