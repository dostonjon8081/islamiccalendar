package uz.def.islamicCalendar.ui.activity

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import uz.def.islamicCalendar.App
import uz.def.islamicCalendar.helper.PreferencesDef
import uz.def.islamicCalendar.helper.db.TimeDataDbFactory
import uz.def.islamicCalendar.helper.db.TimeDataDbViewModel
import uz.def.islamicCalendar.home.api.ApiCalendarViewModel
import uz.def.islamicCalendar.ui.fragment.BaseFragment
import java.lang.Exception

abstract class BaseActivity : AppCompatActivity() {

    val pref by lazy { PreferencesDef(this) }
    val apiCalendarViewModel: ApiCalendarViewModel by lazy { ViewModelProvider(this).get(
        ApiCalendarViewModel::class.java) }

    val timeDataDbViewModel: TimeDataDbViewModel by viewModels {
        TimeDataDbFactory((application as App).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getActivityLayout())


        setupTitle()
//        setupViews()
    }

    abstract fun getFragmentContainerId(): Int

    abstract fun getActivityLayout(): Int

    abstract fun setupTitle()

    abstract fun setupViews()

    fun addFragment(fragment: BaseFragment, container: Int) {
        val fragmentManager = supportFragmentManager.beginTransaction()
        fragmentManager.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)

        fragmentManager.replace(container, fragment)

        try {
            fragmentManager.commit()
        } catch (e: Exception) {
            fragmentManager.commitAllowingStateLoss()
        }
    }

    fun isOnline(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                } else {
                    connectivityManager.getNetworkCapabilities(connectivityManager.allNetworks[0])
                }
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    return true
                }
            }
        }
        return false
    }
}
