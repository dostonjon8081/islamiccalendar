//package uz.def.islamiccalerndar.helper.db
//
//import androidx.room.TypeConverter
//import com.google.gson.Gson
//import com.google.gson.reflect.TypeToken
//import uz.def.islamiccalerndar.home.api.model.DateModel
//import uz.def.islamiccalerndar.home.api.model.MetaModel
//import uz.def.islamiccalerndar.home.api.model.TimeData
//import uz.def.islamiccalerndar.home.api.model.TimingsModel
//
//class Converters {
//    val gson = Gson()
//
//    @TypeConverter
//    fun stringToList(data: String): List<TimeDataModelDb> {
//        if (data == null) return emptyList()
//        val listType = object : TypeToken<List<TimeDataModelDb>>() {}.type
//        return gson.fromJson(data, listType)
//    }
//
//    @TypeConverter
//    fun listToString(timeDataModelDb: List<TimeDataModelDb>): String {
//        return gson.toJson(timeDataModelDb)
//    }
//}
//
//class TimingsConverter {
//    val gson = Gson()
//
//    fun fromString(value: String): TimingsModel {
//        return gson.fromJson(value, TimingsModel::class.java)
//    }
//
//    fun toString(value:TimingsModel):String{
//        return gson.toJson(value,TimingsModel::class.java)
//    }
//}
//class DateConverter {
//    private val gson = Gson()
//
//    fun fromString(value: String): DateModel {
//        return gson.fromJson(value, DateModel::class.java)
//    }
//
//    fun toString(value:DateModel):String{
//        return gson.toJson(value,DateModel::class.java)
//    }
//}
//class MetaConverter {
//    private val gson = Gson()
//
//    fun fromString(value: String): MetaModel {
//        return gson.fromJson(value, MetaModel::class.java)
//    }
//
//    fun toString(value:MetaModel):String{
//        return gson.toJson(value,MetaModel::class.java)
//    }
//}
