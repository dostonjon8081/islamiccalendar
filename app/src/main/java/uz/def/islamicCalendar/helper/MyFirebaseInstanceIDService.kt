package uz.def.islamicCalendar.helper

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

class MyFirebaseInstanceIDService :FirebaseInstanceIdService(){

    override fun onTokenRefresh() {
       val refreshToken =  FirebaseInstanceId.getInstance().token
        Log.d("fireBase","$refreshToken")
    }

}
