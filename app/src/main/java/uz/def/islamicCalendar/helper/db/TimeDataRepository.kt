package uz.def.islamicCalendar.helper.db

class TimeDataRepository(private val timeDataDao: TimeDataDao) {

    fun allTimeData(): List<PrayerTimesModelDb> {
        return timeDataDao.getTimeData()
    }

    suspend fun insertTimeData(timeDataModelDb: PrayerTimesModelDb) {
        timeDataDao.insertTimeData(timeData = timeDataModelDb)
    }

    suspend fun clearDb(){
        timeDataDao.clearTimeData()
    }
}