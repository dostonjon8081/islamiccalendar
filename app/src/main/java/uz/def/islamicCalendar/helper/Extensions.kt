package uz.def.islamicCalendar.helper

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import uz.def.islamicCalendar.ui.activity.BaseActivity
import java.text.SimpleDateFormat

fun permissionCheck(context: Context, accessPermission: String): Boolean {
    val permission = ActivityCompat.checkSelfPermission(context, accessPermission)
    return permission == PackageManager.PERMISSION_GRANTED
}

fun BaseActivity.mGetColor(color: Int): Int {
    return ContextCompat.getColor(applicationContext, color)
}

fun timeToMillisecond(namazTime: String, date: String): Long {
    return SimpleDateFormat("HH:mm dd-MM-yyyy").parse("$namazTime $date").time
}
