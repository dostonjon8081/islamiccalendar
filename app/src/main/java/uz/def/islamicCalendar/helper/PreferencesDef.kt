package uz.def.islamicCalendar.helper

import android.content.Context

class PreferencesDef(private val context: Context) {

    var lastTime: Long
        get() = getLastTime(CONST_LAST_TIME, 0)
        set(lastTime) = setLastTime(CONST_LAST_TIME, lastTime)

    var startState: Boolean
        get() = getStartState(START_STATE, false)
        set(startState) = setStartState(START_STATE, startState)

    var month: Long
        get() = getMonth(CONST_MONTH, 1)
        set(monthPref) = setMonth(CONST_MONTH, monthPref)

    var year: Long
        get() = getYear(CONST_YEAR, 1)
        set(yearPref) = setYear(CONST_YEAR, yearPref)

    var visiblity_bottom_navigation: Boolean
        get() = getVisibilityBottomBar(ON_HOME, false)
        set(onHome) = setVisibilityBottomBar(ON_HOME, onHome)

    var city: String
        get() = getCityName(CITY_NAME, "Uzbekistan")
        set(cityName) = setCityName(CITY_NAME, cityName)

    var firstInit: Boolean
        get() = getFirstInit(FIRST_INIT, false)
        set(firstInit) = setFirstInit(FIRST_INIT, firstInit)

    var latitude: Float
        get() = getLatitude(CONST_LATITUDE, 41.21f)
        set(latitude) = setLatitude(CONST_LATITUDE, latitude)

    var longitude: Float
        get() = getLongitude(CONST_LONGITUDE, 69.25f)
        set(longitude) = setLongitude(CONST_LONGITUDE, longitude)

    private fun setYear(key: String, year: Long) {
        val yearPref = context.getSharedPreferences(PREFERENCES_NAME, 0).edit()
        yearPref.putLong(key, year).apply()
    }

    private fun getYear(key: String, def: Long): Long {
        val yearPref = context.getSharedPreferences(PREFERENCES_NAME, 0)
        return yearPref.getLong(key, def) ?: 1
    }

    private fun setLastTime(key: String, lastTime: Long) {
        val lastTimePref = context.getSharedPreferences(PREFERENCES_NAME, 0).edit()
        lastTimePref.putLong(key, lastTime).apply()
    }

    private fun getLastTime(key: String, def: Long): Long {
        val lastTimePref = context.getSharedPreferences(PREFERENCES_NAME, 0)
        return lastTimePref.getLong(key, def) ?: 1
    }

    private fun setMonth(key: String, month: Long) {
        val monthPref = context.getSharedPreferences(PREFERENCES_NAME, 0).edit()
        monthPref.putLong(key, month).apply()
    }

    private fun getMonth(key: String, def: Long): Long {
        val monthPref = context.getSharedPreferences(PREFERENCES_NAME, 0)
        return monthPref.getLong(key, def) ?: 1
    }

    private fun setVisibilityBottomBar(key: String, onHome: Boolean) {
        val onHomePref = context.getSharedPreferences(PREFERENCES_NAME, 0).edit()
        onHomePref.putBoolean(key, onHome)
        onHomePref.apply()
    }

    private fun getVisibilityBottomBar(key: String, def: Boolean): Boolean {
        val onHomePref = context.getSharedPreferences(PREFERENCES_NAME, 0)
        return onHomePref.getBoolean(key, def) ?: def
    }

    private fun setLongitude(key: String, long: Float) {
        val longitudePref = context.getSharedPreferences(PREFERENCES_NAME, 0).edit()
        longitudePref.putFloat(key, long)
        longitudePref.apply()
    }

    private fun getLongitude(key: String, long: Float): Float {
        val longitudePref = context.getSharedPreferences(PREFERENCES_NAME, 0)
        return longitudePref.getFloat(key, long) ?: long
    }

    private fun setLatitude(key: String, lat: Float) {
        val latitudePref = context.getSharedPreferences(PREFERENCES_NAME, 0).edit()
        latitudePref.putFloat(key, lat)
        latitudePref.apply()
    }

    private fun getLatitude(key: String, lat: Float): Float {
        val latitudePref = context.getSharedPreferences(PREFERENCES_NAME, 0)
        return latitudePref.getFloat(key, lat) ?: lat
    }

    private fun setFirstInit(key: String, value: Boolean) {
        val initSetting = context.getSharedPreferences(PREFERENCES_NAME, 0).edit()
        initSetting.putBoolean(key, value)
        initSetting.apply()
    }

    private fun getFirstInit(key: String, def: Boolean): Boolean {
        val initSetting = context.getSharedPreferences(PREFERENCES_NAME, 0)
        return initSetting.getBoolean(key, def) ?: false
    }

    private fun setCityName(key: String, cityName: String) {
        val cityPref = context.getSharedPreferences(PREFERENCES_NAME, 0).edit()
        cityPref.putString(key, cityName)
        cityPref.apply()
    }

    private fun getCityName(key: String, def: String): String {
        val cityPref = context.getSharedPreferences(PREFERENCES_NAME, 0)
        return cityPref.getString(key, def) ?: "Uzbekistan"
    }

    private fun setStartState(key: String, startState: Boolean) {
        val cityPref = context.getSharedPreferences(PREFERENCES_NAME, 0).edit()
        cityPref.putBoolean(key, startState)
        cityPref.apply()
    }

    private fun getStartState(key: String, def: Boolean): Boolean {
        val cityPref = context.getSharedPreferences(PREFERENCES_NAME, 0)
        return cityPref.getBoolean(key, def) ?: false
    }
}