package uz.def.islamicCalendar.helper

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber

const val CHANNEL_ID = "CHANNEL_ID"
const val NOTIFICATION_ID = 10

class FireBaseMessageService : FirebaseMessagingService() {

    override fun onNewToken(appToken: String) {
        super.onNewToken(appToken)
        Log.d("fireBase ", "$appToken")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        if (remoteMessage.data.isNotEmpty()) {
            Log.d("fireBase ", "${remoteMessage.data}")
            startMyForeground(
                remoteMessage.notification!!.title!!,
                remoteMessage.notification!!.body!!,
                remoteMessage.notification!!.icon!!
            )

        } else Log.d("fireBase ", "massage null")
        if (remoteMessage.notification != null) {
            Timber.d("fireBase: " + remoteMessage?.notification?.body);
        }
    }

    private fun startMyForeground(
        title: String,
        description: String,
        icon: String
    ) {
        val soudUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notifyService = NotificationManagerCompat.from(this)

        val builder = NotificationCompat.Builder(this, CHANNEL_ID).apply {
            setOngoing(true)
                .setContentText(description)
                .setSound(soudUri)
                .setContentTitle(title)
            priority = Notification.PRIORITY_LOW
        }
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(CHANNEL_ID, "name", NotificationManager.IMPORTANCE_DEFAULT)
            notifyService.createNotificationChannel(channel)
        }

        notifyService.notify(0, builder.build())
    }
}
