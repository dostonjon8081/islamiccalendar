package uz.def.islamicCalendar.helper.db

import android.util.Log
import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class TimeDataDbViewModel(private val repository: TimeDataRepository) : ViewModel() {

//    val allTimeData = repository.allTimeData

    private val _liveData = MutableLiveData<List<PrayerTimesModelDb>>()
    val liveData = _liveData

    fun allTimeData(){
        CoroutineScope(Dispatchers.IO).launch {
        liveData.postValue(repository.allTimeData())}
    }

    fun insertTimeData(timeDataModelDb: PrayerTimesModelDb) = viewModelScope.launch {
        repository.insertTimeData(timeDataModelDb)
    }

    fun clearDb()=viewModelScope.launch{
        repository.clearDb()
    }

}

class TimeDataDbFactory(private val repository: TimeDataRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(TimeDataDbViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return TimeDataDbViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}