package uz.def.islamicCalendar.helper.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [PrayerTimesModelDb::class], version = 1)
//@TypeConverters(Converters::class)
abstract class TimeDataBase : RoomDatabase() {

    abstract fun timeDataDao(): TimeDataDao

    companion object {
        @Volatile
        var INSTANCE: TimeDataBase? = null

        fun getDatabase(context: Context): TimeDataBase {

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    TimeDataBase::class.java,
                    "timeDataStorage.db"
                )
                    .build()

                INSTANCE = instance
                return INSTANCE!!
            }
        }
    }
}