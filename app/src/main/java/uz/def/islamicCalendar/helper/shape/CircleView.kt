package uz.def.islamicCalendar.helper.shape

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.view.View
import java.util.jar.Attributes

class CircleView(context: Context, attributes: Attributes) : View(context) {
    var paint: Paint? = null
    var canvas: Canvas? = null
//    val array  = mutableListOf<Float>()

    init {
        paint = Paint()
        canvas = Canvas()
        paint?.color = Color.BLACK
        paint?.isAntiAlias = true
        paint?.strokeWidth = 5f
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        this.canvas?.drawCircle(10f,10f,10f,paint!!)
    }
}