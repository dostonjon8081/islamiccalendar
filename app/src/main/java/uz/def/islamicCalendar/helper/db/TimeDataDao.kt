package uz.def.islamicCalendar.helper.db

import androidx.room.*

@Dao
interface TimeDataDao {
    @Query("""select * from PRAYER_DATE_TABLE""")
    fun getTimeData(): List<PrayerTimesModelDb>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertTimeData(timeData: PrayerTimesModelDb)

    @Query("""delete from PRAYER_DATE_TABLE""")
    suspend fun clearTimeData()
}