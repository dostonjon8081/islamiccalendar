package uz.def.islamicCalendar.helper.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "prayer_date_table")
data class PrayerTimesModelDb(
    @ColumnInfo(name = "fajr")
    @SerializedName("fajr")
    val fajr: String,

    @ColumnInfo(name = "sunrise")
    @SerializedName("sunrise")
    val sunrise: String,

    @ColumnInfo(name = "dhuhr")
    @SerializedName("dhurh")
    val dhurh: String,

    @ColumnInfo(name = "asr")
    @SerializedName("asr")
    val asr: String,

    @ColumnInfo(name = "magrib")
    @SerializedName("magrib")
    val magrib: String,

    @ColumnInfo(name = "isha")
    @SerializedName("isha")
    val isha: String,


    @ColumnInfo(name = "date")
    @SerializedName("date")
    val date: String,


    @ColumnInfo(name = "month")
    @SerializedName("month")
    val month: Int,

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Long = 0


)