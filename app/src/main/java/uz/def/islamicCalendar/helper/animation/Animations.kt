package uz.def.islamicCalendar.helper.animation

import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation

fun View.animationSplash(
    start: Float,
    end: Float,
    duration: Long,
    animationEnd: () -> (Unit)
) {
    val anim = ScaleAnimation(
        start, end,
        start, end,
        Animation.RELATIVE_TO_SELF, 0.5f,
        Animation.RELATIVE_TO_SELF, 0.5f
    )
    anim.fillAfter = true
    anim.duration = duration
    anim.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(p0: Animation?) {}

        override fun onAnimationEnd(p0: Animation?) {
            animationEnd()
        }

        override fun onAnimationStart(p0: Animation?) {}
    })

    this.startAnimation(anim)
}