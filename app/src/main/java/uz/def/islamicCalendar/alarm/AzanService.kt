package uz.def.islamicCalendar.alarm

import android.app.*
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.IBinder
import android.util.Log
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import uz.def.islamicCalendar.App
import uz.def.islamicCalendar.R
import uz.def.islamicCalendar.helper.*
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class AzanService : Service() {

    private val data by lazy { (application as App).repository.allTimeData() }
    private val pref by lazy { PreferencesDef(this) }
    private val executors: ExecutorService = Executors.newFixedThreadPool(1)


    override fun onCreate() {
        startMyForeground()
        Log.d("servicess", "onCreate: started service")
        executors.execute { MyRunnable().run() }
        stopSelf()
        super.onCreate()
    }

    override fun onBind(intent: Intent?): IBinder? = null
    private fun startMyForeground() {
        val notifyService = NotificationManagerCompat.from(this)
        val remoteViews =
            RemoteViews(this.packageName, uz.def.islamicCalendar.R.layout.custom_notif)
        val builder = NotificationCompat.Builder(this, CHANNEL_ID).apply {
            setContent(remoteViews)
            setOngoing(true)
//            setSmallIcon(R.drawable.ic_service_notification)
            priority = Notification.PRIORITY_LOW
        }
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(CHANNEL_ID, "name", NotificationManager.IMPORTANCE_DEFAULT)
            notifyService.createNotificationChannel(channel)
        }
        startForeground(NOTIFICATION_ID, builder.build())
        stopSelf()
    }

    inner class MyRunnable : Runnable {
        override fun run() {

            Thread.sleep(10_000)
            readData()
            stopForeground(true)
            stopSelf()
        }

        private fun readData(): Long {
            val currentTime = Date().time
            val currentDate = Date().day - 1
            val dailyData = data[currentDate]

            val fajr = timeToMillisecond(dailyData.fajr, dailyData.date)
            val sunrise = timeToMillisecond(dailyData.sunrise, dailyData.date)
            val dhuhr = timeToMillisecond(dailyData.dhurh, dailyData.date)
            val asr = timeToMillisecond(dailyData.asr, dailyData.date)
            val magrib = timeToMillisecond(dailyData.magrib, dailyData.date)
            val isha = timeToMillisecond(dailyData.isha, dailyData.date)
            val nextFajr =
                timeToMillisecond(data[currentDate + 1].fajr, data[currentDate + 1].date)
            /*    val fajr = 2000L
                val sunrise = 3000L
                val dhuhr = 4000L
                val asr = 5000L
                val magrib = 6000L
                val isha = 7000L
                val nextFajr =
                    10000L

                when (2000L) {
                    fajr -> pref.lastTime = 2000L
                    sunrise -> pref.lastTime = 3000L
                    dhuhr -> pref.lastTime = 3000L
                    asr ->  pref.lastTime =4000L
                    magrib -> pref.lastTime = 5000L
                    isha -> pref.lastTime = 6000L
                }*/
            when (pref.lastTime) {
                fajr -> pref.lastTime = sunrise
                sunrise -> pref.lastTime = dhuhr
                dhuhr -> pref.lastTime = asr
                asr -> pref.lastTime = magrib
                magrib -> pref.lastTime = isha
                isha -> pref.lastTime = nextFajr
            }

            val intent = Intent(this@AzanService, AlarmReceiver::class.java)
            if (pref.lastTime == sunrise) {
                intent.putExtra(SOUND_NOTIF, R.raw.sunrice)
            } else {
                intent.putExtra(SOUND_NOTIF, R.raw.mansoor_azan)
            }
            intent.action = CONST_ALAR_RECEIVER_INTENT
            val pendingIntent =
                PendingIntent.getBroadcast(applicationContext, 234324243, intent, 0)
            val alarmManager =
                getSystemService(Context.ALARM_SERVICE) as AlarmManager

            alarmManager[AlarmManager.RTC_WAKEUP, pref.lastTime] = pendingIntent
            return pref.lastTime

        }
    }
}