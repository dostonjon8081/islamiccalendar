package uz.def.islamicCalendar.alarm

import android.app.ActivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.util.Log
import androidx.core.content.ContextCompat
import uz.def.islamicCalendar.R
import uz.def.islamicCalendar.helper.CONST_ALAR_RECEIVER_INTENT
import uz.def.islamicCalendar.helper.SOUND_NOTIF
import java.util.*
import java.util.concurrent.Executors

class AlarmReceiver : BroadcastReceiver() {
    //    val data by lazy { (application as App).repository.allTimeData() }
    val executors = Executors.newSingleThreadExecutor()
    val calen: Calendar = Calendar.getInstance()

    override fun onReceive(context: Context?, intent: Intent?) {

        if (!isMyServiceRunning(AzanService::class.java,context!!)){

        intent.let { intent ->
            val bundle = intent?.extras
            if (bundle!=null){
                val sound = bundle?.getInt(SOUND_NOTIF)
//                val eventForAzan = bundle?.getString(CONST_ALAR_RECEIVER_INTENT)

                if (intent?.action == CONST_ALAR_RECEIVER_INTENT){
                    Intent(context, AzanService::class.java).apply {
                        ContextCompat.startForegroundService(context!!, this)
                    }
                    MediaPlayer.create(context,sound).start()
                }
                if (intent?.action == "android.intent.action.BOOT_COMPLETED"){
                    Intent(context, AzanService::class.java).apply {
                        ContextCompat.startForegroundService(context!!, this)
                    }
                }
            }
        }}
    }

    private fun isMyServiceRunning(serviceClass: Class<*>,context: Context): Boolean {
        val manager =
            context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
        for (service in manager!!.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                Log.i("isMyServiceRunning?", true.toString() + "")
                return true
            }
        }
        Log.i("isMyServiceRunning?", false.toString() + "")
        return false
    }
}

