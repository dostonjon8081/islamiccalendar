package uz.def.islamicCalendar.firstInit

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import kotlinx.android.synthetic.main.fragment_first_init.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.DisposableHandle
import kotlinx.coroutines.launch
import uz.def.islamicCalendar.helper.PreferencesDef
import uz.def.islamicCalendar.R
import uz.def.islamicCalendar.helper.animation.ZoomRecyclerLayout
import uz.def.islamicCalendar.ui.activity.MainActivity
import uz.def.islamicCalendar.ui.fragment.BaseFragment
import java.text.SimpleDateFormat
import java.util.*

class FirstInitFragment : BaseFragment() {

    var regionsAdapter: RegionsAdapter? = null
    val linearLayoutManager by lazy { ZoomRecyclerLayout(context!!) }
    val regionArray = mutableListOf<RegionModel>()
    val pref by lazy { PreferencesDef(context!!) }
    override fun getLayout(): Int = R.layout.fragment_first_init

    override fun setTitle() {

        regionArray.add(RegionModel(getString(R.string.tashkent), 41.2795f, 69.3401f))
        regionArray.add(RegionModel(getString(R.string.andijan), 40.8154f, 72.334f))
        regionArray.add(RegionModel(getString(R.string.bukhara), 39.7681f, 64.4556f))
        regionArray.add(RegionModel(getString(R.string.fergana), 40.371049f, 71.794575f))
        regionArray.add(RegionModel(getString(R.string.sirdaryo), 40.444827f, 68.836030f))
        regionArray.add(RegionModel(getString(R.string.jizzakh), 40.124600f, 67.811573f))
        regionArray.add(RegionModel(getString(R.string.namangan), 40.996289f, 71.590016f))
        regionArray.add(RegionModel(getString(R.string.navoiy), 40.098345f, 65.328203f))
        regionArray.add(RegionModel(getString(R.string.karakalpakstan), 42.441868f, 59.616252f))
        regionArray.add(RegionModel(getString(R.string.tashkentreg), 40.949174f, 69.939289f))
        regionArray.add(RegionModel(getString(R.string.qashqadaryo), 38.831810f, 65.784465f))
        regionArray.add(RegionModel(getString(R.string.samarqand), 39.581510f, 66.959898f))
        regionArray.add(RegionModel(getString(R.string.surkhandaryo), 37.263983f, 67.399686f))
        regionArray.add(RegionModel(getString(R.string.xorazm), 41.531416f, 60.658939f))
        regionArray.sortBy { it.region }
        regionArray.add(RegionModel("", 0.0f, 0.0f))
        regionArray.add(RegionModel("", 0.0f, 0.0f))
        regionArray.add(RegionModel("", 0.0f, 0.0f))
    }

    override fun setViews() {
        setUpRegionRecyclerView()
    }

    private fun setUpRegionRecyclerView() {
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rvRegions.layoutManager = linearLayoutManager
        rvRegions.hasFixedSize()
        regionsAdapter = RegionsAdapter(regionArray, context!!)
        rvRegions.adapter = regionsAdapter

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(rvRegions)
        rvRegions.isNestedScrollingEnabled = false

        regionsAdapter?.onClick = {
            pref.latitude = it.latitude
            pref.longitude = it.longitude
            pref.month = SimpleDateFormat("MM", Locale.getDefault()).format(Date()).toLong()
            pref.year = SimpleDateFormat("yyyy", Locale.getDefault()).format(Date()).toLong()
            getBaseActivity {base->
                pref.firstInit = true
                pref.visiblity_bottom_navigation = true

                (base as MainActivity).bottomBarVisibility()
                base.firstRequestApiCalendar()
               /* CoroutineScope(Dispatchers.IO).launch {
                        base.timeDataDbViewModel.allTimeData()
                }*/
                base.goHomeLayout()
//                it.addFragment(HomeFragment(),it.getFragmentContainerId())
            }
        }
    }
}