package uz.def.islamicCalendar.firstInit

data class RegionModel(val region: String, val latitude: Float, val longitude: Float)