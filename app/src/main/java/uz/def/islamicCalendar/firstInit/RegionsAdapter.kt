package uz.def.islamicCalendar.firstInit

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import uz.def.islamicCalendar.R

class RegionsAdapter(private val regions: MutableList<RegionModel>, context: Context) :
    RecyclerView.Adapter<RegionsAdapter.RegionsVH>() {

    var onClick: ((RegionModel) -> Unit)? = null

    val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegionsVH {
        return RegionsVH(inflater.inflate(R.layout.item_regions_adapter, parent, false))
    }

    override fun getItemCount(): Int = regions.size


    override fun onBindViewHolder(holder: RegionsVH, position: Int) {

        holder.apply {
            title.text = regions[position].region
        }
        holder.itemView.setOnClickListener {
            if (regions[position].region != "") {
                onClick?.invoke(regions[position])
            }
        }
    }

    class RegionsVH(private val containerView: View) : RecyclerView.ViewHolder(containerView) {
        val title = containerView.findViewById<AppCompatTextView>(R.id.textRegions)
    }
}