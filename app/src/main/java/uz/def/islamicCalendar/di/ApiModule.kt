//package uz.def.islamiccalerndar.di
//
//import dagger.Module
//import dagger.Provides
//import retrofit2.Converter
//import retrofit2.Retrofit
//import retrofit2.converter.gson.GsonConverterFactory
//import uz.def.islamiccalerndar.helper.BASE_URL
//import uz.def.islamiccalerndar.home.api.ApiCalendar
//import javax.inject.Singleton
//
//@Module
//object ApiModule {
//
//    @JvmStatic
//    @Provides
//    @Singleton
//    fun providerApiCalendar(): ApiCalendar {
//        return Retrofit.Builder()
//            .addConverterFactory(provideConverter())
//            .baseUrl(BASE_URL)
//            .build()
//            .create(ApiCalendar::class.java)
//    }
//
//    private fun provideConverter(): Converter.Factory {
//        return GsonConverterFactory.create()
//    }
//}
