package uz.def.islamicCalendar

import android.app.Application
import uz.def.islamicCalendar.helper.db.TimeDataBase
import uz.def.islamicCalendar.helper.db.TimeDataRepository

open class App : Application() {

    private val dataBase by lazy { TimeDataBase.getDatabase(this) }
    val repository by lazy { TimeDataRepository(dataBase.timeDataDao()) }
}