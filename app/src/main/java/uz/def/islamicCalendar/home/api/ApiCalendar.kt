package uz.def.islamicCalendar.home.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import uz.def.islamicCalendar.home.api.model.PrayerCalendarModel

interface ApiCalendar {
    @GET("calendar?")
      fun reminderCalendar(
        @Query("latitude") latitude: Float,
        @Query("longitude") longitude: Float,
        @Query("method") method: Long,
        @Query("school") school: Long,
        @Query("month") month: Long,
        @Query("year") year: Long
    ):Call<PrayerCalendarModel>
}