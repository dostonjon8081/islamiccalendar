package uz.def.islamicCalendar.home.api.model


data class PrayerCalendarModel(
    val code:Long,
    val status:String,
    val data:MutableList<TimeData>
)

data class TimeData (
    val timings: TimingsModel,
    val date: DateModel,
    val meta: MetaModel
)
