package uz.def.islamicCalendar.home.api.model

import com.google.gson.annotations.SerializedName

data class DateModel(
    @SerializedName("readable")
    val readable: String,
    @SerializedName("timestamp")
    val timestamp: String,
    @SerializedName("gregorian")
    val gregorian: CalendarGregorian,
    val hijri: Hijri
//    @SerializedName("")
)

data class CalendarGregorian(
    val date: String,
    val format: String,
    val day: String,
    val weekday: WeekDay,
    val month: MonthI,
    val year: String,
    val designation: Designation,
    val holidays: MutableList<Any>
)

data class Hijri(
    val date: String,
    val format: String,
    val day: String,
    val weekday: WeekDay,
    val month: MonthI,
    val year: String,
    val designation: Designation,
    val holidays: MutableList<Any>
)

data class WeekDay (
    val en: String,
    val ar: String
)

data class MonthI(
    val number: Long,
    @SerializedName("en")
    val en: String,
    val ar: String
)

data class Designation(
    val abbreviated: String,
    val expanded: String
)
