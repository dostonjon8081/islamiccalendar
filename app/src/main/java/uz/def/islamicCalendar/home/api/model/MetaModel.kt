package uz.def.islamicCalendar.home.api.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class MetaModel(
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double,
    @SerializedName("timeZone")
    val timeZone: String,
    @SerializedName("method")
    val method: Method,
    @SerializedName("latitudeAdjustmentMethod")
    val latitudeAdjustmentMethod: String,
    @SerializedName("midnightMode")
    val midnightMode: String,
    @SerializedName("school")
    val school: String
)

data class Method(
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("params")
    val params: ParamsDef
)

data class ParamsDef(
    @SerializedName("fajr")
    val Fajr: Int,
    @SerializedName("Isha")
    val Isha: Int
)

data class Offset(
    @SerializedName("imsak")
    @Expose
    val Imsak: Int,
    @SerializedName("fajr")
    @Expose
    val Fajr: Int,
    @SerializedName("Sunrise")
    val Sunrise: Int,
    @SerializedName("dhuhr")
    val Dhuhr: Int,
    @SerializedName("asr")
    val Asr: Int,
    @SerializedName("maghrib")
    val Maghrib: Int,
    @SerializedName("sunset")
    val Sunset: Int,
    @SerializedName("isha")
    val Isha: Int,
    @SerializedName("midnight")
    val Midnight: Int
)