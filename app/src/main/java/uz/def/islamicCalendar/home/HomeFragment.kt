package uz.def.islamicCalendar.home

import android.annotation.SuppressLint
import android.text.format.DateFormat
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.prayer_times.*
import kotlinx.android.synthetic.main.top_time.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import uz.def.islamicCalendar.R
import uz.def.islamicCalendar.helper.timeToMillisecond
import uz.def.islamicCalendar.ui.activity.MainActivity
import uz.def.islamicCalendar.ui.fragment.BaseFragment
import java.util.*

class HomeFragment : BaseFragment() {

    var day: Int? = null
    var relicTime = 0
    override fun getLayout(): Int = R.layout.fragment_home
    override fun setTitle() {
        getBaseActivity {
            day = DateFormat.format("dd", Date()).toString().toInt() - 1
            it.pref.visiblity_bottom_navigation = true
        }
    }

    override fun setViews() {
        CoroutineScope(Dispatchers.Main).async {
            readFromDatabase()
        }.onAwait

    }

    private fun readFromDatabase() {

        getBaseActivity { base ->
            if (!base.pref.firstInit) {
                getApiCalendar()
                base.pref.firstInit = true
            } else {
                val month = DateFormat.format("MM", Date()).toString().toInt()
                base.timeDataDbViewModel.liveData.observe(owner = this@HomeFragment) {
                    it.let {
                        if (it.isEmpty()) {
                            getApiCalendar()
                        } else {
                            if (it[0].month != month) {
                                (base as MainActivity).firstRequestApiCalendar()
                            } else {

                                checkTime(
                                    it[this.day!!].fajr.substring(0, 5),
                                    it[this.day!!].sunrise.substring(0, 5),
                                    it[this.day!!].dhurh.substring(0, 5),
                                    it[this.day!!].asr.substring(0, 5),
                                    it[this.day!!].magrib.substring(0, 5),
                                    it[this.day!!].isha.substring(0, 5),
                                    it[this.day!! + 1].fajr.substring(0, 5),
                                    it[this.day!!].date
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    private fun getApiCalendar() {
        getBaseActivity { base ->

            base.apiCalendarViewModel?.apiCalendar?.observe(this, Observer {
                val prayerData = it.data[this.day!!].date.gregorian.date
                val prayerTimings = it.data[this.day!!].timings

                val fajr = prayerTimings.Fajr.substring(0, 5)
                val sunrise = prayerTimings.Sunrise.substring(0, 5)
                val dhuhr = prayerTimings.Dhuhr.substring(0, 5)
                val asr = prayerTimings.Asr.substring(0, 5)
                val maghrib = prayerTimings.Maghrib.substring(0, 5)
                val isha = prayerTimings.Isha.substring(0, 5)
                val currentDate = prayerData

                checkTime(
                    fajr, sunrise, dhuhr, asr, maghrib, isha,
                    it.data[this.day!! + 1].timings.Fajr.substring(0, 5), currentDate
                )
            })
        }
    }

    private fun checkTime(
        substringFajr: String,
        substringSunrise: String,
        substringDhurh: String,
        substringAsr: String,
        substringMaghrib: String,
        substringIsha: String,
        substringNextFajr: String,
        currentDate: String
    ) {
        // bottom times
        fajr_time.text = substringFajr
        dhurh_time.text = substringDhurh
        asr_time.text = substringAsr
        magrib_time.text = substringMaghrib
        isha_time.text = substringIsha

        requireContext().apply {
            val fajr = timeToMillisecond(substringFajr, currentDate)
            val sunrise = timeToMillisecond(substringSunrise, currentDate)
            val dhuhr = timeToMillisecond(substringDhurh, currentDate)
            val asr = timeToMillisecond(substringAsr, currentDate)
            val maghrib = timeToMillisecond(substringMaghrib, currentDate)
            val isha = timeToMillisecond(substringIsha, currentDate)
            val nextFajr =
                timeToMillisecond(substringNextFajr, currentDate)


            when (Calendar.getInstance().time.time) {
                in fajr..sunrise -> setPrayerTime(fajr, sunrise, "Bomdod", substringFajr)
                in sunrise..dhuhr -> setPrayerTime(sunrise, dhuhr, "Quyosh", substringSunrise)
                in dhuhr..asr -> setPrayerTime(dhuhr, asr, "Peshin", substringDhurh)
                in asr..maghrib -> setPrayerTime(asr, maghrib, "Asr", substringAsr)
                in maghrib..isha -> setPrayerTime(fajr, sunrise, "Shom", substringMaghrib)
                in isha..nextFajr -> setPrayerTime(fajr, sunrise, "Hufton", substringIsha)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setPrayerTime(
        first: Long,
        second: Long,
        timeName: String,
        startedTime: String
    ) {

        prayerTimeName.text = timeName
        prayerTime.text = startedTime

        val result = second - first

        top_progress.max = (result / 1000 / 60).toInt()

        top_progress.progress = ((Date().time - first) / 1000 / 60).toInt()


        getBaseActivity {
            (it as MainActivity).setProgressCounter(top_progress.max + 1, top_progress.progress)
            topProgressBar()

            it.pref.lastTime = second
        }

        gregorianTime.text = DateFormat.format("dd", Date()).toString() + " " +
                DateFormat.format("MMMM", Date()).toString() + "\n" +
                DateFormat.format("EEEE", Date()).toString()

        tally_text.text = DateFormat.format("dd/MM/yyyy", Date()).toString()
    }

    private fun topProgressBar() {
        CoroutineScope(Dispatchers.IO).launch {
            CoroutineScope(Dispatchers.Main).async {
                getBaseActivity { it ->

                    it.apiCalendarViewModel?.liveData?.observe(
                        this@HomeFragment,
                        Observer {
                            top_progress.progress = it.toInt()

                            relicTime = top_progress.max - it.toInt()

                            progress_number.text = "- ${relicTime / 60}:${relicTime % 60}"
                            progress_hour.text =
                                DateFormat.format("HH:mm", Date()).toString()

                            if (it.toInt() >= top_progress.max) {
                                readFromDatabase()
                            }
                        })
                }
            }
        }
    }
}
