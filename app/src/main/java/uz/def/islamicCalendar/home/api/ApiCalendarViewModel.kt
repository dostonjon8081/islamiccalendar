package uz.def.islamicCalendar.home.api

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uz.def.islamicCalendar.home.api.model.PrayerCalendarModel

//class ApiCalendarViewModel(val prayerCalendarRepository: PrayerCalendarRepository) : ViewModel() {
class ApiCalendarViewModel : ViewModel() {


    private val prayerCalendarRepository by lazy { PrayerCalendarRepository() }
    private val _apiCalendar = MutableLiveData<PrayerCalendarModel>()
    val apiCalendar: LiveData<PrayerCalendarModel> = _apiCalendar

    private val _liveData = MutableLiveData<String>()
    val liveData: LiveData<String> = _liveData


    fun provideCalendar(
        latitude: Float,
        longitude: Float,
        month: Long,
        year: Long
    ) {
        prayerCalendarRepository.getPrayerCalendarApi(latitude, longitude, month, year) {
            Log.d("getCalendar", "$it")
            _apiCalendar.postValue(it)
        }
    }

    //    private val progressCounter= ProgressCounter(_liveData)
    fun runProgress(count: Int, progress: Int) {
        for (it in progress..count) {
            _liveData.postValue(it.toString())
            Thread.sleep(1000 * 60)
        }
    }
}
