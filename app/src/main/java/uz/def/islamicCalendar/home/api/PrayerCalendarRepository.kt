package uz.def.islamicCalendar.home.api

import android.util.Log
import kotlinx.coroutines.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uz.def.islamicCalendar.helper.BASE_URL
import uz.def.islamicCalendar.home.api.model.PrayerCalendarModel
import java.lang.Exception

class PrayerCalendarRepository() {

    fun getPrayerCalendarApi(
        latitude: Float,
        longitude: Float,
        month: Long,
        year: Long,
        method: Long = 2,
        school: Long = 1,
        onSuccess: (PrayerCalendarModel) -> Unit
    ) {

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val apiCalendar = getRetrofit()

                val result =
                    apiCalendar.reminderCalendar(latitude, longitude, method,school, month, year).execute()
                if (result.isSuccessful) {
                    if (result.body() != null) {
                        Log.d("getCalendar", "${result.body()}")
                        onSuccess(result.body()!!)
                    }
                }
            } catch (e: Exception) {
            }
        }
    }

    companion object {

        var INSTANCE: ApiCalendar? = null
        private fun getRetrofit(): ApiCalendar {

            if (INSTANCE==null){
               INSTANCE = Retrofit.Builder()
//            .client(OkHttpClient())
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(ApiCalendar::class.java)
            }

            return INSTANCE!!
        }

    }

}