package uz.def.islamicCalendar.compass

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.Log
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_compass.*
import uz.def.islamicCalendar.R
import uz.def.islamicCalendar.ui.fragment.BaseFragment

class CompassActivity : BaseFragment(), SensorEventListener {


/*  private lateinit var sensorManager: SensorManager
private var mLight: Sensor? = null
var currentDegree: Float = -90f

override fun getFragmentContainerId(): Int {
return R.id.container
}

override fun getActivityLayout(): Int {
return R.layout.activity_compass
}

override fun setupTitle() {
sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
mLight = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
}

override fun setupViews() {
}

override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
// Do something here if sensor accuracy changes.
}

override fun onSensorChanged(event: SensorEvent) {
// The light sensor returns a single value.
// Many sensors return 3 values, one for each axis.
val degree = event.values[0]
val animation = RotateAnimation(
currentDegree,
-degree, Animation.RELATIVE_TO_SELF, 0.5f,
Animation.RELATIVE_TO_SELF, 0.5f
)
//        Log.d("compasss", "$animation")
Log.d("compasss", "event")

animation.duration = 500
animation.fillAfter = true
iv_compass?.animation = animation


currentDegree = -degree
}

override fun onResume() {
super.onResume()
mLight?.also { light ->
sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)
}
}

override fun onPause() {
super.onPause()
sensorManager.unregisterListener(this)
}
*/

    private val sensorManager by lazy(LazyThreadSafetyMode.NONE) {
        requireContext().getSystemService(
            Context.SENSOR_SERVICE
        ) as SensorManager
    }
    private val accelerometerSensor by lazy { sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) }
    private val magneticFieldSensor by lazy { sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) }
    private val sensor: Sensor by lazy(LazyThreadSafetyMode.NONE) {
        sensorManager.getDefaultSensor(
            Sensor.TYPE_ORIENTATION
        )
    }
    var currentDegree: Float = -90f
//    private var iv_compass: AppCompatImageView? = null

    override fun getLayout() = R.layout.activity_compass

    override fun setTitle() {

/* supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_home)
supportActionBar?.setDisplayHomeAsUpEnabled(true)
supportActionBar?.setDisplayShowTitleEnabled(true)
supportActionBar?.setDisplayShowHomeEnabled(true)

supportActionBar?.setTitle(R.string.app_name)*/

//        iv_compass = findViewById(R.id.iv_compass)
        Log.d("compasss", "${sensor}")
        Log.d("compasss", "in title")
        if (sensor != null) {
            Log.d("compasss", "in if")
            Log.d("compasss", "${sensor}")
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI)

           /* sensorManager.registerListener(
                this,
                accelerometerSensor,
                SensorManager.SENSOR_DELAY_NORMAL
            )
            sensorManager.registerListener(
                this,
                magneticFieldSensor,
                SensorManager.SENSOR_DELAY_NORMAL
            )*/

        } else Toast.makeText(requireContext(), "not supported", Toast.LENGTH_LONG).show()

    }

    override fun setViews() {

    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {}

    override fun onSensorChanged(event: SensorEvent?) {

        val degree = event!!.values[0]
        val animation = RotateAnimation(
            currentDegree, -degree,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
        )
//        Log.d("compasss", "$animation")
        Log.d("compasss", "event")

        animation.duration = 200
        animation.fillAfter = true
//        animation.interpolator = LinearInterpolator()
//        iv_compass?.rotation = degree
        currentDegree = -degree
        iv_compass?.startAnimation(animation)
//        iv_compass.rotation = currentDegree


    }

    override fun onResume() {
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_MAGNETIC_FIELD)
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_ACCELEROMETER)
        super.onResume()
    }


    override fun onPause() {
        sensorManager.unregisterListener(this)
        super.onPause()
    }
}

/*
class CompassActivity : BaseActivity(), SensorEventListener {


    *//*  private lateinit var sensorManager: SensorManager
      private var mLight: Sensor? = null
      var currentDegree: Float = -90f

      override fun getFragmentContainerId(): Int {
          return R.id.container
      }

      override fun getActivityLayout(): Int {
         return R.layout.activity_compass
      }

      override fun setupTitle() {
          sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
          mLight = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
      }

      override fun setupViews() {
      }

      override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
          // Do something here if sensor accuracy changes.
      }

      override fun onSensorChanged(event: SensorEvent) {
          // The light sensor returns a single value.
          // Many sensors return 3 values, one for each axis.
          val degree = event.values[0]
          val animation = RotateAnimation(
              currentDegree,
              -degree, Animation.RELATIVE_TO_SELF, 0.5f,
              Animation.RELATIVE_TO_SELF, 0.5f
          )
  //        Log.d("compasss", "$animation")
          Log.d("compasss", "event")

          animation.duration = 500
          animation.fillAfter = true
          iv_compass?.animation = animation


          currentDegree = -degree
      }

      override fun onResume() {
          super.onResume()
          mLight?.also { light ->
              sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)
          }
      }

      override fun onPause() {
          super.onPause()
          sensorManager.unregisterListener(this)
      }
  *//*

    private val sensorManager by lazy(LazyThreadSafetyMode.NONE) { getSystemService(Context.SENSOR_SERVICE) as SensorManager }
    private val accelerometerSensor by lazy { sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) }
    private val magneticFieldSensor by lazy { sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) }
    private val sensor: Sensor by lazy(LazyThreadSafetyMode.NONE) {
        sensorManager.getDefaultSensor(
            Sensor.TYPE_ORIENTATION
        )
    }
    var currentDegree: Float = -90f
    private var iv_compass: AppCompatImageView? = null

    override fun getFragmentContainerId() = R.id.container

    override fun getActivityLayout() = R.layout.activity_compass

    override fun setupTitle() {

        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_home)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        supportActionBar?.setTitle(R.string.app_name)



        iv_compass = findViewById(R.id.iv_compass)
        Log.d("compasss", "${sensor}")
        Log.d("compasss", "in title")
        if (sensor != null) {
            Log.d("compasss", "in if")
            Log.d("compasss", "${sensor}")
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST)

            sensorManager.registerListener(
                this,
                accelerometerSensor,
                SensorManager.SENSOR_DELAY_NORMAL
            )
            sensorManager.registerListener(
                this,
                magneticFieldSensor,
                SensorManager.SENSOR_DELAY_NORMAL
            )

        } else Toast.makeText(this, "not supported", Toast.LENGTH_LONG).show()


    }

    override fun setupViews() {}

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {}

    override fun onSensorChanged(event: SensorEvent?) {

        val degree = event!!.values[0]
        val animation = RotateAnimation(
            currentDegree,
            -degree, Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
        )
//        Log.d("compasss", "$animation")
        Log.d("compasss", "event")

        animation.duration = 500
        animation.fillAfter = true
        iv_compass?.animation = animation


        currentDegree = -degree
    }

    override fun onResume() {

        sensor.also { light ->
            sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)
        }
        super.onResume()
    }

    override fun onPause() {
        sensorManager.unregisterListener(this)
        super.onPause()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}*/

/**
 *
: BaseFragment(), SensorEventListener {


/*  private lateinit var sensorManager: SensorManager
private var mLight: Sensor? = null
var currentDegree: Float = -90f

override fun getFragmentContainerId(): Int {
return R.id.container
}

override fun getActivityLayout(): Int {
return R.layout.activity_compass
}

override fun setupTitle() {
sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
mLight = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
}

override fun setupViews() {
}

override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
// Do something here if sensor accuracy changes.
}

override fun onSensorChanged(event: SensorEvent) {
// The light sensor returns a single value.
// Many sensors return 3 values, one for each axis.
val degree = event.values[0]
val animation = RotateAnimation(
currentDegree,
-degree, Animation.RELATIVE_TO_SELF, 0.5f,
Animation.RELATIVE_TO_SELF, 0.5f
)
//        Log.d("compasss", "$animation")
Log.d("compasss", "event")

animation.duration = 500
animation.fillAfter = true
iv_compass?.animation = animation


currentDegree = -degree
}

override fun onResume() {
super.onResume()
mLight?.also { light ->
sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)
}
}

override fun onPause() {
super.onPause()
sensorManager.unregisterListener(this)
}
*/

private val sensorManager by lazy(LazyThreadSafetyMode.NONE) { requireContext().getSystemService(Context.SENSOR_SERVICE) as SensorManager }
private val accelerometerSensor by lazy { sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) }
private val magneticFieldSensor by lazy { sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) }
private val sensor: Sensor by lazy(LazyThreadSafetyMode.NONE) {
sensorManager.getDefaultSensor(
Sensor.TYPE_ORIENTATION
)
}
var currentDegree: Float = -90f
//    private var iv_compass: AppCompatImageView? = null

override fun getLayout() = R.layout.activity_compass

@SuppressLint("LogNotTimber")
override fun setTitle() {

/* supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_home)
supportActionBar?.setDisplayHomeAsUpEnabled(true)
supportActionBar?.setDisplayShowTitleEnabled(true)
supportActionBar?.setDisplayShowHomeEnabled(true)

supportActionBar?.setTitle(R.string.app_name)*/

//        iv_compass = findViewById(R.id.iv_compass)
Log.d("compasss", "${sensor}")
Log.d("compasss", "in title")
if (sensor != null) {
Log.d("compasss", "in if")
Log.d("compasss", "${sensor}")
sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST)

//            sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL)
//            sensorManager.registerListener(this, magneticFieldSensor, SensorManager.SENSOR_DELAY_NORMAL)

} else Toast.makeText(requireContext(), "not supported", Toast.LENGTH_LONG).show()

}

override fun setViews() {

}

override fun onAccuracyChanged(p0: Sensor?, p1: Int) {}

override fun onSensorChanged(event: SensorEvent?) {

val degree = event!!.values[0]
val animation = RotateAnimation(
currentDegree,
-degree, Animation.RELATIVE_TO_SELF, 0.5f,
Animation.RELATIVE_TO_SELF, 0.5f
)
//        Log.d("compasss", "$animation")
Log.d("compasss", "event")

animation.duration = 500
animation.fillAfter = true
iv_compass?.animation = animation


currentDegree = -degree
}

override fun onResume() {

sensor.also { light ->
sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)}
super.onResume()
}




override fun onPause() {
sensorManager.unregisterListener(this)
super.onPause()
}

//    override fun onSupportNavigateUp(): Boolean {
//        onBackPressed()
//        return true
//    }

}*/