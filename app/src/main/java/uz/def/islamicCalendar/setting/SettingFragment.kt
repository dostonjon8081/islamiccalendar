package uz.def.islamicCalendar.setting

import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_setting.*
import uz.def.islamicCalendar.R
import uz.def.islamicCalendar.ui.activity.BaseActivity
import uz.def.islamicCalendar.ui.activity.MainActivity
import uz.def.islamicCalendar.ui.fragment.BaseFragment

class SettingFragment : BaseFragment() {

    override fun getLayout() = R.layout.fragment_setting


    override fun setTitle() {
        btn_location.setOnClickListener {
            if (verifyAvailableNetwork(requireActivity() as BaseActivity)) {
                getBaseActivity {
                    (it as MainActivity).checkPermissionLocation()
                }
            } else {
                Toast.makeText(requireContext(), "You have not network ", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun setViews() {
    }

    private fun verifyAvailableNetwork(activity: BaseActivity): Boolean {
        val connectivityManager =
            activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (permissions.isNotEmpty() &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            getBaseActivity {
                (it as MainActivity).checkEnableLocation()
            }
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}